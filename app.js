
const express = require('express');
const http = require('http');
var path = require('path');
var bodyParser = require('body-parser');

var multer = require('multer');
var upload = multer({dest: 'public/images/'});
var expressValidator = require('express-validator');
const hostname = '127.0.0.1';


const port = 300;



var app = express();
const server = http.Server(app);

// View Engine
app.set('view engine','ejs');
app.set('views',path.join(__dirname,'views'));
//Body Parser Middleware
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());

//Set static path
app.use(express.static(path.join(__dirname,'public')));

// Express Validator Middleware
app.use(expressValidator({
  errorFormatter: function(param, msg, value) {
      var namespace = param.split('.')
      , root    = namespace.shift()
      , formParam = root;

    while(namespace.length) {
      formParam += '[' + namespace.shift() + ']';
    }
    return {
      param : formParam,
      msg   : msg,
      value : value
    };
  }
}));

var routes = require('./routes/routes');

app.use('/',routes);

server.listen(port,hostname, () =>{
	console.log("Server running at http://" + hostname + ":" +port);
});


