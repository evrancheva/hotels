var itemWraper = $('.button');
var cityName = $('.city-name');

var itemWraper_OnHover = function() {
    if (!$(this).children('.btn').hasClass("active-btn")) {
        $(".btn").removeClass("active-btn");
        $(this).children('.btn').addClass('active-btn');

        cityName.text($(this).children('.btn').val());



    } else {
        $(this).children('.btn').removeClass('active-btn');
        cityName.text("");
    }



}

itemWraper.click(itemWraper_OnHover);

function ShowContent() {
    
    $('.content').show();
}

function Weather(location) {

    $.simpleWeather({

        location: location,
        unit: 'c',
        success: function(weather) {
            html = '<h2>' + weather.city + ', ' + weather.region + '</h2>';
            html += '<img style="float:left;" width="125px" src="images/weather/' + weather.code + '.png">';
            html += '<p>' + weather.temp + '&deg; ' + weather.units.temp + '<br /><span>' + weather.currently + '</span></p>';


            $("#weather").html(html);
        },
        error: function(error) {
            $("#weather").html("<p>" + error + "</p>");
        }
    });
}


function getExternal(city) {


       var city =
            "http://api.flickr.com/services/feeds/photos_public.gne?" +
            "tags=+" + city + "&format=json&jsoncallback=?";

    $.getJSON(city, function(flickrResponse) {
        /*var url =
            "http://api.flickr.com/services/feeds/photos_public.gne?" +
            "tags=+" + city + "&format=json&jsoncallback=?";*/
        var count = 0;

        var images = new Array();
        flickrResponse.items.map(function(item) {
            count++;
            if (count <= 9) {
                images.push(item.media.m);
            }
        });
        
        
        // Putting images on the right places 
        // for big images
            var bigImage = images[0].replace("_m.jpg","_c.jpg");
            $('.main-img').attr("src",bigImage);
        
            for(var a = 1; a < images.length; a++){
                
                AddSrcAndOnclick(a,images[a]);
            }
    


    });
}
/*function getExternalTweets() {
     var url = "https://api.twitter.com/1.1/search/tweets.json?q=braga&count=4";
  $.getJSON(url, function (flickrResponse) {
  // we'll simply print the response to the console // for the time being console.log(flickrResponse);
    console.log(flickrResponse);
    // iterrate response and add for each item one line -- see html


    
     /*   tweeterResponse.items.map(function(item) {
            count++;
            if (count <= 9) {
                images.push(item.media.m);
            }
        });
        
      
        // Putting images on the right places 
        // for big images
    


    });
}
  */
function AddSrcAndOnclick(number,image){
      $('.img'+number).attr("src",image);
   $('.img'+number).attr("onclick","ChangeMainPicture("+ "'" + image+ "'" +");");
    
}
function ChangeMainPicture(src){
   
     var bigImage = src.replace("_m.jpg","_c.jpg");
    $('.changable').fadeOut(400, function() {
            $(".changable").attr('src',bigImage);
        })
        .fadeIn(400);
   //   $('.changable').attr("src",src);
}

function ChangeCityName(name){
    $('.cityName').text(name);
    var nameWithoutSpace = name.replace(" ","");
    $('.tophotels').attr('href','/BestHotels/'+nameWithoutSpace + '/');
  $('.promotions').attr('href','/TopPromotions/'+nameWithoutSpace + '/');
    /*
    $('.cityName').each(function(i, obj) {
 
    obj.text(name);
});*/
}