const express = require('express');
let PLayerAPI = require('./server/players');

function initialize(){
	let api = express();
	api.use('/team',PLayerAPI());

	return api;
}
module.exports = {
	initialize:initialize,
};